package com.study.board.controller;

import com.study.board.entity.Board;
import com.study.board.service.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/*
@XXX : 어노테이션
  - JEE5(Java Platform, Enterprise Edition 5)부터 새롭게 추가된 요소
  - 이 어노테이션으로 인해 데이터의 유효성 검사 등을 쉽게 알 수 있고, 이와 관련한 코드가 깔끔해지게 됩니다.
    일단 어노테이션의 용도는 다양한 목적이 있지만 메타 데이터의 비중이 가장 크다 할 수 있습니다.
  - 메타-테이터(Meta-Data) : 데이터를 위한 데이터를 의미하며, 풀어 이야기하면 한 데이터에 대한 설명을 의미하는 데이터. (자신의 정보를 담고 있는 데이터)

@Controller : 해당 어노테이션이 적용된 클래스는 "Controller"임을 나타나고, bean으로 등록되며 해당 클래스가 Controller로 사용됨을 Spring Framework에 알립니다
  - @RequestMapping 어노테이션을 해당 어노테이션 밑에서만 사용할 수 있다.

@ResponseBody : http 본문자체를 읽는 부분을 모델로 변환시켜주는 어노테이션이다
  - 이를 쓰지않고 return 값에 HelloWorld라고 쓰면 HelloWorld라는 View 파일을 찾는다.
  - view 페이지가 아닌 HelloWorld 라는 문자열을 그대로 반환하고 싶다면, 해당 메소드 위에 @ResponseBody를 추가한다
  - 문자열이 아닌 VO 객체를 반환해도 된다. 객체를 반환하게 되면, json 형태로 반환한다.
  - @RestController를 사용하면, 아래처럼 @ResponseBody 어노테이션을 붙이지 않아도 된다.

@GetMapping : HTTP GET 요청이 올 때만 자바 메소드가 실행됩니다. (데이터를 가져올 때 사용)
  - GetMapping 어노테이션은 @RequestMapping(method=RequestMethod.GET, value=..) 의 축약형 표현입니다.
*/


/*
[단축키] 
# Shift + F10 : RUN 실행
*/

@Controller
public class BoardController {

    /* TEST 'Hello World' */
    @GetMapping("/test")
    @ResponseBody
    public String main(){
        return "Hello World";
    }


    /* 게시판 기본 */
    @GetMapping("/board")
    public String board_index(){ return "redirect:/board/list"; }


    /* 게시판 등록 폼 ================================================== */
    @GetMapping("/board/write")
    public String board_write_form(){
        return "board/board_write";
    }



    /* 게시판 등록 프로세스 ============================================= */
    @Autowired
    private BoardService bds;

    @PostMapping("/board/write_do")
    public String board_write_process(Board bd, Model md, MultipartFile file) throws Exception{

        bds.serviceBoardWrite(bd, file);

        // + 등록 메시지 적용
        md.addAttribute("message", "글 작성이 완료되었습니다.");
        md.addAttribute("searchUrl", "/board/list");

        return "board_message";
    }

    /* 게시판 등록 프로세스 ============================================= 메시지 적용 후 프로세스 (2) + 업로드 적용 전
    @PostMapping("/board/write_do")
    public String board_write_process(Board bd, Model md) {
        bds.serviceBoardWrite(bd);

        // + 등록 메시지 적용
        md.addAttribute("message", "글 작성이 완료되었습니다.");
        md.addAttribute("searchUrl", "/board/list");

        return "board_message";
    } */

    /* 게시판 등록 프로세스 ============================================= 메시지 적용 전 등록 프로세스 (1)
    //public String write_process(String title, String content) {               // 이렇게 파라미터를 받을수도 있으나 너무 많으면 처리가 힘듬
    public String board_write_process(Board bd) {                               // Entity Board (com.study.board.entity.Board)

        bds.serviceBoardWrite(bd);
        return "redirect:/board/list";
    } */


    /* 게시판 리스트 =================================================== 
    * localhost:8080/board/list?page=1&size=10
    * 필요변수   
       - nowPage : 현재페이지
       - startPage : 블럭에서 보여줄 시작페이지
       - endPage : 블럭에서 보여줄 마지막페이지
    */
    @GetMapping("/board/list")
    public String board_list(Model md
                            , @PageableDefault(page = 0, size = 10, sort = "id", direction = Sort.Direction.DESC) Pageable pageable
                            , String searchKeyword) {                                // Model 매개변수

        Page<Board> list = null;

        // # 검색 처리
        if(searchKeyword == null) {
            list =  bds.serviceBoardList(pageable);
        } else {
            list =  bds.serviceBoardSearchList(searchKeyword, pageable);
        }

        // # 페이징 처리
        //int nowPage = pageable.getPageNumber();                   // 현재 페이지 정보를 가져옴
        int nowPage = list.getPageable().getPageNumber() + 1;       // 현재페이지 : 페이저블에서 넘어온 현재 페이지 정보 (페이저블이 가지고있는 페이지는 0에서 시작하므로 1을 더함)
        int startPage = Math.max(nowPage - 4, 1);                   // 시작페이지 : Math.max 을 써서 1보다 작을 경우 1을 반환하도록 함
        int endPage = Math.min(nowPage + 5, list.getTotalPages());  // 마지막페이지 : Math.min 을 써서 최대 페이지가 총 페이지 수를 넘지 않도록 함
        int totalPage = list.getTotalPages();

        md.addAttribute("list", list);
        md.addAttribute("nowPage", nowPage);
        md.addAttribute("startPage", startPage);
        md.addAttribute("endPage", endPage);
        md.addAttribute("totalPage", totalPage);

        return "board/board_list";
    }

    /*
    /* 게시판 리스트 =================================================== 검색처리 전 (2)
    * localhost:8080/board/list?page=1&size=10
    * 필요변수
       - nowPage : 현재페이지
       - startPage : 블럭에서 보여줄 시작페이지
       - endPage : 블럭에서 보여줄 마지막페이지
    *
    @GetMapping("/board/list")
    public String board_list(Model md, @PageableDefault(page = 0, size = 10, sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {                                // Model 매개변수

        Page<Board> list =  bds.serviceBoardList(pageable);

        //int nowPage = pageable.getPageNumber();                   // 현재 페이지 정보를 가져옴
        int nowPage = list.getPageable().getPageNumber() + 1;       // 현재페이지 : 페이저블에서 넘어온 현재 페이지 정보 (페이저블이 가지고있는 페이지는 0에서 시작하므로 1을 더함)
        int startPage = Math.max(nowPage - 4, 1);                   // 시작페이지 : Math.max 을 써서 1보다 작을 경우 1을 반환하도록 함
        int endPage = Math.min(nowPage + 5, list.getTotalPages());  // 마지막페이지 : Math.min 을 써서 최대 페이지가 총 페이지 수를 넘지 않도록 함
        int totalPage = list.getTotalPages();

        md.addAttribute("list", list);
        md.addAttribute("nowPage", nowPage);
        md.addAttribute("startPage", startPage);
        md.addAttribute("endPage", endPage);
        md.addAttribute("totalPage", totalPage);

        return "board_list";
    }
    */

    /* 게시판 리스트 =================================================== 페이징 처리 전 (1)
    @GetMapping("/board/list")
    public String board_list(Model md) {                                // Model 매개변수

        md.addAttribute("list", bds.serviceBoardList());
        return "board_list";
    }
    */


    /* 게시판 상세 ===================================================== */
    @GetMapping("/board/view")
    public String board_view(Model md, Integer id) {                    // http://localhost:8080/board/view?id=1

        md.addAttribute("board_data", bds.serviceBoardView(id));
        return "board/board_view";
    }


    /* 게시판 삭제 ===================================================== */
    @GetMapping("/board/delete_do")                                     // http://localhost:8080/board/delete_do?id=1  (쿼리 스트링)
    public String board_delete(Integer id) {

        bds.serviceBoardDelete(id);
        return "redirect:/board/list";
    }


    /* 게시판 수정 폼 ===================================================== (@PathVariable) 사용 예제 */
    @GetMapping("/board/update/{id}")                                               // http://localhost:8080/board/update/1 (패스 베리어블)
    public String board_update_form(@PathVariable("id") Integer id, Model md){      // @GetMapping의 {id}가 @PathVariable("id")로 인식되어 Integer id로 담아줌

        md.addAttribute("board_data", bds.serviceBoardView(id));
        return "board/board_update";
    }


    /* 게시판 수정 프로세스 ================================================ (@PathVariable) 사용 예제 */
    @PostMapping("/board/update_do/{id}")
    public String board_update_process(@PathVariable("id") Integer id, Board bd, MultipartFile file) throws Exception {

        // 기존 내용에 새로운 내용을 덮어씌운다
        Board boardTemp = bds.serviceBoardView(id);
        boardTemp.setTitle(bd.getTitle());
        boardTemp.setContent(bd.getContent());

        bds.serviceBoardWrite(boardTemp, file);
        return "redirect:/board/list";
    }

    /* 게시판 수정 프로세스 ================================================ 업로드 적용 전 (1)
    public String board_update_process(@PathVariable("id") Integer id, Board bd){

        // 기존 내용에 새로운 내용을 덮어씌운다
        Board boardTemp = bds.serviceBoardView(id);
        boardTemp.setTitle(bd.getTitle());
        boardTemp.setContent(bd.getContent());

        bds.serviceBoardWrite(boardTemp);
        return "redirect:/board/list";
    }
    */
}
