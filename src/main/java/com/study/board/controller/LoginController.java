package com.study.board.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.study.board.core.SHA256;
import com.study.board.entity.Member;
import com.study.board.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

@Controller
public class LoginController {

    @Autowired
    private LoginService lgs;


    /* 로그인 기본 */
    @GetMapping("/login")
    public String login_index(){
        return "login";
    }


    /* 로그인 처리 ================================================== */
    @PostMapping("/login/ajax_login_do")
    @ResponseBody
    public String login_process(@RequestParam Map<String, String> map, HttpServletRequest request) throws NoSuchAlgorithmException {

        Boolean passEqualsResult = false;       // 비번 일치여부
        String loginAuth = "none";              // 로그인 권한

        // 로그인 정보
        String userid = map.get("userid");
        String password = map.get("password");

        //SHA256으로 비밀번호 암호화 처리
        SHA256 sha256 = new SHA256();
        String secretPass = sha256.encrypt(password);

        // 회원정보 찾기
        Member mbr = lgs.serviceLoginUserid(userid);

        if(mbr == null) {
            // 회원 아님
            loginAuth = "problem_userid";
        } else {
            // 비밀번호 일치여부
            passEqualsResult =  secretPass.equals(mbr.getPassword());

            if(passEqualsResult == false) {
                // 비밀번호 불일치
                loginAuth = "problem_password";
            } else {
                loginAuth = "success";
                //System.out.println(mbr.getUserid());
                //System.out.println(mbr.getPassword());
            }
        }

        // 로그인 처리
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        if(loginAuth == "problem_userid") {
            obj.addProperty("result", "false");
            obj.addProperty("message", "존재하지 않는 아이디입니다.");
        } else if(loginAuth == "problem_password") {
            obj.addProperty("result", "false");
            obj.addProperty("message", "비밀번호가 일치하지 않습니다.");
        } else if(loginAuth == "success") {
            setSession(request, mbr);     // 세션처리
            obj.addProperty("result", "true");
            obj.addProperty("message", "로그인 되었습니다.");
        } else {
            obj.addProperty("result", "false");
            obj.addProperty("message", "올바르지 않은 경로입니다.");
        }

        // JsonObject를 Json 문자열로 변환
        String jsonStr = gson.toJson(obj);

        // 생성된 Json 문자열 출력
        return obj.toString();
    }


    /* 세션 정보 추가 ================================================== */
    public void setSession(HttpServletRequest request, Member mer) {
        HttpSession session = request.getSession(true);

        session.setAttribute("idx", mer.getIdx());
        session.setAttribute("userid", mer.getUserid());
        session.setAttribute("username", mer.getUsername());
        session.setAttribute("email", mer.getEmail());
        session.setMaxInactiveInterval(-1);   // 세션유지시간 설정

        System.out.println("☆ LOGIN SESSION - (userid : " + session.getAttribute("userid") + ")");
    }


    /* 로그 아웃 ====================================================== */
    @GetMapping("/login/logout")
    public String logout_process(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        session.invalidate();

        if(session == null) {
            return "redirect:/login";
        } else {
            return "redirect:/main";
        }
    }


}
