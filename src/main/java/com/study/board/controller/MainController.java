package com.study.board.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    /* 메인 페이지 */
    @GetMapping("/")
    public String main_inedx(){
        return "redirect:/main";
    }

    /* 메인 페이지 */
    @GetMapping("/main")
    public String main_info(){
        return "main";
    }

}

