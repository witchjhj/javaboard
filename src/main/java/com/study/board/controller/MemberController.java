package com.study.board.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.study.board.core.SHA256;
import com.study.board.entity.Member;
import com.study.board.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;


@Controller
public class MemberController {

    @Autowired
    private MemberService mbs;

    /* 회원 기본 */
    @GetMapping("/member")
    public String member_index(){ return "redirect:/member/list"; }


    /* 회원 리스트 */
    @GetMapping("/member/list")
    public String member_list(Model md
            , @PageableDefault(page = 0, size = 10, sort = "idx", direction = Sort.Direction.DESC) Pageable pageable
            , @RequestParam(value = "userid", required = false, defaultValue = "") String userid
            , @RequestParam(value = "username", required = false, defaultValue = "") String username ) {

        Page<Member> list = null;

        // # 검색 처리
        //if(userid == null && username == null) {
        if(userid == "" && username == "") {
            list =  mbs.serviceMemberList(pageable);
        } else {
            list =  mbs.serviceMemberSearchList(userid, username, pageable);
        }

        // # 페이징 처리
        //int nowPage = pageable.getPageNumber();                   // 현재 페이지 정보를 가져옴
        int nowPage = list.getPageable().getPageNumber() + 1;       // 현재페이지 : 페이저블에서 넘어온 현재 페이지 정보 (페이저블이 가지고있는 페이지는 0에서 시작하므로 1을 더함)
        int startPage = Math.max(nowPage - 4, 1);                   // 시작페이지 : Math.max 을 써서 1보다 작을 경우 1을 반환하도록 함
        int endPage = Math.min(nowPage + 5, list.getTotalPages());  // 마지막페이지 : Math.min 을 써서 최대 페이지가 총 페이지 수를 넘지 않도록 함
        int totalPage = list.getTotalPages();

        md.addAttribute("list", list);
        md.addAttribute("nowPage", nowPage);
        md.addAttribute("startPage", startPage);
        md.addAttribute("endPage", endPage);
        md.addAttribute("totalPage", totalPage);

        return "member/member_list";
    }


    /* 게시판 등록 폼 ================================================== */
    @GetMapping("/member/write/{id}")
    public String member_write_form(@PathVariable("id") Integer id, Model md){

        if(id == 0) {
            // 등록
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("idx", "0");
            map.put("userid", "");
            map.put("password", "");
            map.put("username", "");
            map.put("email", "");

            md.addAttribute("member_data", map);
        } else {
            // 수정
            md.addAttribute("member_data", mbs.serviceMemberView(id));
        }

        return "member/member_write";
    }



    /* 게시판 등록 프로세스 ============================================= */
    @PostMapping("/member/ajax_write_do/{id}")
    @ResponseBody                                          // ajax 처리할 경우 @RequestBody 어노테이션을 넣어야 한다.
    public String member_write_process(@PathVariable("id") Integer id, Member mbr)  throws NoSuchAlgorithmException {

        String pros = "save";

        // 비밀번호 암호화
        SHA256 sha256 = new SHA256();
        String sha256pw = sha256.encrypt(mbr.getPassword());

        // 등록/수정
        if(id == 0) {
            // 등록
            String id_check = check_userid(mbr.getUserid());
            if(id_check == "Y") {
                pros = "id_exist";
            } else {
                mbr.setPassword(sha256pw);
                mbs.serviceMemberWrite(mbr);
            }
        } else {
            // 수정
            Member mTemp = mbs.serviceMemberView(id);
            mTemp.setPassword(sha256pw);
            mTemp.setUsername(mbr.getUsername());
            mTemp.setEmail(mbr.getEmail());

            mbs.serviceMemberWrite(mTemp);
        }

        // GsonBuilder
        Gson gson = new Gson();

        // Json key, value 추가
        JsonObject obj = new JsonObject();
        if(pros == "id_exist") {
            obj.addProperty("result", "false");
            obj.addProperty("message", "이미 존재하는 아이디입니다.");
        } else {
            obj.addProperty("result", "true");
            obj.addProperty("message", "등록되었습니다.");
        }

        // JsonObject를 Json 문자열로 변환
        String jsonStr = gson.toJson(obj);

        // 생성된 Json 문자열 출력
        //System.out.println(jsonStr); // {"result":"true", "message":"등록되었습니다"}
        return obj.toString();
    }


    /* [Check] 아이디 존재유무 체크 ----------------- ☆ */
    public String check_userid (String userid) {
        Integer cnt = mbs.serviceMemberUseridCheck(userid);

        String rs = "N";

        // 아이디가 이미 있네요~
        if(cnt > 0) { rs = "Y"; }

        return rs;
    }
}
