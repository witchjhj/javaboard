package com.study.board.controller;

import com.google.gson.JsonObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.swing.*;
import java.util.Enumeration;


/* 예제(1) static 정적 메소드 확인용 ===========================================================  : START */
class StaticNumber{
    static int num = 0;         // 클래스 필드
    int num2 = 0;               // 인스턴스 필드
}
class StaticName{
    static void testStaticPrint() { // 클래스 메소드
        System.out.println("내 이름은 홍길동입니다.");
    }

    void testStaticPrint2() { // 인스턴스 메소드
        System.out.println("내 이름은 이순신입니다.");
    }
}
/* 예제(1) static 정적 메소드 확인용 ===========================================================  : END */



@Controller
public class TestController {

    /* (1) static 정적 메소드 =================================================================================================== : START
        * Static 영역에 할당된 메모리는 모든 객체가 공유하여 하나의 멤버를 어디서든지 참조할 수 있는 장점을 가지지만
          Garbage Collector의 관리 영역 밖에 존재하기에 Static영역에 있는 멤버들은 프로그램의 종료시까지 메모리가 할당된 채로 존재하게 된다.
          그렇기에 Static을 너무 남발하게 되면 만들고자 하는 시스템 성능에 악영향을 줄 수 있다
    */
    @GetMapping("/test/static")
    @ResponseBody
    public String test_01_static (){

        // # 숫자로 확인
        StaticNumber number1 = new StaticNumber();
        StaticNumber number2 = new StaticNumber();

                System.out.println("number1 num: " + number1.num);          // 결과 : 0
                System.out.println("number1 num2: " + number1.num2);        // 결과 : 0
                System.out.println("number2 num: " + number2.num);          // 결과 : 0
                System.out.println("number2 num2: " + number2.num2);        // 결과 : 0

        number1.num++;      // 첫번째 number
        number1.num2++;     // 두번째 number

                System.out.println("add number1 num: " + number1.num);      // 결과 : 1
                System.out.println("add number1 num2: " + number1.num2);    // 결과 : 1
                System.out.println("add number2 num: " + number2.num);      // 결과 : 1
                System.out.println("add number2 num2: " + number2.num2);    // 결과 : 0


        // # 텍스트로 확인
        // - 정적 메소드(static)는 클래스가 메모리에 올라갈때 자동으로 생성되므로 인스턴스를 생성하지 않아도 호출이 가능 / 정적 메소드는 유틸리티 함수를 만드는데 유용하게 사용됨
        StaticName.testStaticPrint();

        // - 인스턴스를 생성 (정적 메소드가 아닌 경우에는 인스턴스 생성이 반드시 필요
        StaticName nm = new StaticName();
        nm.testStaticPrint2();

        return "[@ TestController] : (1) Java Static TEST";
    }
    /* (1) static 정적 메소드 =================================================================================================== : END */


    @GetMapping("/test/request")
    @ResponseBody
    public String test_request(HttpServletRequest request, Model md){

        System.out.println("getMethod: " + request.getMethod()); //  http 메서드 반환 => GET
       // System.out.println(request.getProtocal()); //  프로토콜 반환 => HTTP/1.1
        System.out.println("getScheme: " + request.getScheme()); // 스키마 반환 => http
        System.out.println("getRequestURL: " + request.getRequestURL()); // URL 반환 =>http://localst:9090/test
        System.out.println("getRequestURI: " + request.getRequestURI()); // URI 반환 => /test
        System.out.println("getQueryString: " + request.getQueryString()); // 쿼리파라미터 반환
        System.out.println("getQueryString: " + request.isSecure()); // https 사용유무

        /* 헤더 라인 */
        System.out.println("getServerName: " + request.getServerName()); // 서버 이름 반환(Host) => localhost
        System.out.println("getServerPort: " + request.getServerPort()); // 서버 포트번호 반환 => 9090
        System.out.println("getContentType: " + request.getContentType()); // ContentType반한 => null
        System.out.println("getCharacterEncoding: " + request.getCharacterEncoding()); // CharacterEncoding 반환 => UTF-8
        System.out.println("getContentLength: " + request.getContentLength()); // 내용크기 반환

        // 매개변수
        //md.getAttribute("getMethod", request);

        return "test";
    }



    @GetMapping("/test/requests")
    @ResponseBody
    public String test_request_all(HttpServletRequest request){

        Enumeration params = request.getParameterNames();
        System.out.println("----------------------------");
        while (params.hasMoreElements()){
            String name = (String)params.nextElement();
            System.out.println(name + " : " +request.getParameter(name));
        }
        System.out.println("----------------------------");

        return "test";
    }


    @GetMapping("/test/jsonobject")
    @ResponseBody
    public String test_json_object(){
        JsonObject jo = new JsonObject();
        //jo.put("name", "Jone");
        jo.addProperty("name", "Jone");
        jo.addProperty("city", "Seoul");

        System.out.println(jo.toString());

        return "# JsonObject 객체 사용: " + jo.toString();
    }
}
