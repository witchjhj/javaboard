package com.study.board.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class CustomWebMvcConfigurer implements WebMvcConfigurer {   // extends WebMvcConfigurationSupport {

    // addPathPatterns : 추가할 주소
    // excludePathPatterns : 예외 처리할 주소

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginIntercepter())
                //.addPathPatterns("/main/**", "/board/**", "/member/**", "/*")
                .addPathPatterns("/**")
                .excludePathPatterns("/login", "/login/**", "/css/**", "/img/**", "/js/**", "/files", "/logs/**", "/dist/**", "/test/**");

        //*  :  1뎁스
        //** :  1뎁스 그 이상 주소 모든 경로
        //?  :  한개의 글자

        /* registry.addInterceptor(new LoginIntercepter())
                .addPathPatterns("/user/**"); */
    }

}

