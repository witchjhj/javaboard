package com.study.board.core;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@Component
public class LoginIntercepter implements HandlerInterceptor {

    /* preHandler : 컨트롤러 실행 전에 동작하는 메소드로 return값이 true이면 진행, false이면 멈춥니다. */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // Get Session
        HttpSession session = request.getSession();
        String userid = (String) session.getAttribute("userid");

        // TEST - System print
        UrlPathHelper urlPathHelper = new UrlPathHelper();
        String originalURL = urlPathHelper.getOriginatingRequestUri(request);

        if(userid == null || userid == "") {
            System.out.println("[" + originalURL + "] 컨트롤러 요청 - SESSION 정보가 없어 Login 이동");

            response.sendRedirect("/login");
            return false;               // 컨트롤러로 넘어가지 않도록 false 처리
        } else {
            System.out.println("[" + originalURL + "] 컨트롤러 요청 - SESSION (userid : " + userid + ")");
        }

        return true;
    }

    /* postHandler : 컨트롤러에 도착하여 view가 랜더링되기 전에 동작합니다.
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {

    }
    */


    /* afterCompletion : view가 정상적으로 랜더링된 후에 마지막에 실행됩니다. *
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, @Nullable Exception arg3) throws Exception {

    }
    */
}