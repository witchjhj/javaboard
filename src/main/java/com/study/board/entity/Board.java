package com.study.board.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/*
@Entity : JPA를 사용해 테이블과 매핑할 클래스에 붙여주는 어노테이션이다. 이 어노테이션을 붙임으로써 JPA가 해당 클래스를 관리하게 된다.
  - spring-boot-starter-data-jpa 의존성을 추가하고 @Entiy 어노테이션을 붙이면 테이블과 자바 클래스가 매핑이 된다.
    그래서 JPA에서 '하나의 엔티티 타입을 생성한다'라는 의미는 '하나의 클래스를' 작성한다는 의미가 된다.


@Data : @Getter, @Setter, @RequiredArgsConstructor, @ToString, @EqualsAndHashCode을 한꺼번에 설정해주는 매우 유용한 어노테이션입니다.
  - 모든 필드를 대상으로 접근자와 설정자가 자동으로 생성되고, final 또는 @NonNull 필드 값을 파라미터로 받는 생성자가 만들어지며, toStirng, equals, hashCode 메소드가 자동으로 만들어집니다.
*/

@Entity
@Data
public class Board {        // Entity Class 이름은 DB 테이블명과 같으면 좋다.

    /*
    @Id : 특정 속성을 기본키로 설정하는 어노테이션이다.
      - @Id 어노테이션만 적게될 경우 기본키값을 직접 부여해줘야 한다. 하지만 보통 DB를 설계할 때는 기본키는 직접 부여하지 않고 Mysql AUTO_INCREMENT처럼 자동 부여되게끔 한다.

    @GeneratedValue : 어노테이션을 사용하면 기본값을 DB에서 자동으로 생성하는 전략을 사용 할 수 있다. 전략에는 IDENEITY, SEQUENCE, TABLE 3가지가 있다.
      - @GeneratedValue(startegy = GenerationType.IDENTITY) : 기본 키 생성을 DB에 위임 (Mysql)
      - @GeneratedValue(startegy = GenerationType.SEQUENCE) : DB시퀀스를 사용해서 기본 키 할당 (ORACLE)
      - @GeneratedValue(startegy = GenerationType.TABLE) : 키 생성 테이블 사용 (모든 DB 사용 가능)
      - @GeneratedValue(startegy = GenerationType.AUTO) : 선택된 DB에 따라 자동으로 전략 선택
      AUTO 같은 경우DB에 따라 전략을 JPA가 자동으로 선택한다. 이로 인해 DB를 변경해도 코드를 수정할 필요 없다는 장점이 있다.
    */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String content;

    private String filename;
    private String filepath;
}
