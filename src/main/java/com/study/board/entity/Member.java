package com.study.board.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idx;

    @Column(name = "user_id")
    private String userid;

    @Column(name = "user_name")
    private String username;

    @CreationTimestamp
    @Column(name = "join_date", nullable= false, updatable = false)
    private LocalDateTime joindate;


    private String password;
    private String email;

}
