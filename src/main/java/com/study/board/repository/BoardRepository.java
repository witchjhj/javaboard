package com.study.board.repository;

import com.study.board.entity.Board;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
@Repository :  Entity에 의해 생성된 DB에 접근하는 메서드들을 사용하기 위한 인터페이스이다. (퍼시스턴스 레이어, DB나 파일같은 외부 I/O 작업을 처리함)
  - 위에서 엔티티를 선언함으로써 데이터베이스 구조를 만들었다면, 여기에 어떤 값을 넣거나, 넣어진 값을 조회하는 등의 CRUD(Create, Read, Update, Delete)를 해야 쓸모가 있는데,
    이것을 어떻게 할 것인지 정의해주는 계층이라고 생각하면 된다.
  - @Component 어노테이션을 사용해도 상관 없지만 @Repository 어노테이션에 @Component 어노테이션의 기능이 포함되어 있고 @Repository를 사용함으로써
    해당 클래스가 Repositorydml 역할을 하는 것을 명확하게 알 수 있습니다.
  - JpaRepository를 상속받도록 함으로써 기본적인 동작이 모두 가능해진다!
    JpaRepository는 어떤 엔티티를 메서드의 대상으로 할지를 다음 키워드로 지정한다. JpaRepository<대상으로 지정할 엔티티, 해당 엔티티의 PK의 타입>.
  - 이렇게 extends를 통해서 상속받고나면, 해당 레포지토리의 객체를 이용해서 기본적으로 제공되는 메서드(save(), findAll(), get()) 등을 사용할 수 있게 된다.
*/


@Repository
public interface BoardRepository extends JpaRepository<Board, Integer> {           // JpaRepository<대상으로 지정할 엔티티, 해당 엔티티의 PK의 타입>.
    Page<Board> findByTitleContaining(String searchKeyword, Pageable pageable);
}


/* 검색 개발 전 코드
@Repository
public interface BoardRepository extends JpaRepository<Board, Integer> {           // JpaRepository<대상으로 지정할 엔티티, 해당 엔티티의 PK의 타입>.
}
*/