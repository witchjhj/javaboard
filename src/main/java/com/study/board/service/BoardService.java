package com.study.board.service;

import com.study.board.entity.Board;
import com.study.board.repository.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.UUID;

/*
@Service : 해당 클래스가 서비스 레이어 클래스라는 것을 명확히 함
  - 비즈니스 로직이나 respository layer 호출하는 함수에 사용된다.

@Autowired : 스프링 DI(Dependency Injection)에서 사용되는 어노테이션입니다.
  - DI(의존성 종속, Dependency Injection)란, 클래스간의 의존관계를 스프링 컨테이너가 자동으로 연결해주는 것을 말합니다.
  - 스프링에서 빈 인스턴스가 생성된 이후 @Autowired를 설정한 메서드가 자동으로 호출되고, 인스턴스가 자동으로 주입됩니다.
    즉, 해당 변수 및 메서드에 스프링이 관리하는 Bean을 자동으로 매핑해주는 개념입니다.
    @Autowired 는 변수, Setter메서드, 생성자, 일반 메서드에 적용이 가능하며 <property>, <constructor-arg>태그와 동일한 역할을 합니다.
  - @Autowired는 주로 타입(Type)을 이용해 주입합니다. @Autowired 가 Type으로 찾아서 주입하므로 동일한 Bean 타입의 객체가 여러개 있을 경우,
    Bean을 찾기 위해 @Qualifier 어노테이션을 같이 사용해야 합니다.
  - @Autowired로 의존 관계를 주입받을 경우, 해당 클래스(@Autowired당하는 클래스)를 반드시 Component Auto Scanning을 설정해야 합니다.  Component Auto Scanning 코드는 아래와 같습니다.
    <context:component-scan base-package="factory.userfactory" />
*/


/*
    [Spring Data JPA]
    - save()		: 새로운 엔티티는 저장( persist )하고 이미 있는 엔티티는 수정합니다.
                  ( 수정된 필드만 수정 되는 것이 아니라 모든 필드에 대해서 업데이트가 진행됩니다. )
    - ﬁndAll()	: 모든 엔티티를 조회합니다.
    - delete() 	: 엔티티 하나를 삭제합니다.
    - ﬁndOne()	: 엔티티 하나를 조회합니다
    - getOne()	: 엔티티가 아닌 프록시로 조회합니다.
*/


@Service
public class BoardService {

    @Autowired
    private BoardRepository bdRepository;

    /* 글 등록 처리 =================================================================== */
    public void serviceBoardWrite(Board bd, MultipartFile file) throws Exception {       // 리턴없음 void | filename은 view의 input type이 File인 것의 이름과 동일해야 함

        // 파일업로드
        String projectPath = System.getProperty("user.dir") + "\\src\\main\\resources\\static\\files";      // System.getProperty("user.dir") : 프로젝트 경로
        UUID uid = UUID.randomUUID();                                   // 랜덤 UUID(식별자)를 생성
        String fileName = uid + "_" + file.getOriginalFilename();       // 랜덤 UUID + "_" + 원래 파일명
        File saveFile = new File(projectPath, fileName);                // "File saveFile"하는 빈 "new File" 파일을 생성할건데, 경로는 "projectPath"이고, 이름은 "fileName"이다
        file.transferTo(saveFile);

        bd.setFilename(file.getOriginalFilename());
        bd.setFilepath("/files/" + fileName);                           // static 밑에있는 부분은 그 하위경로만으로도 접근이 가능하다

        // 저장
        bdRepository.save(bd);
    }

    /* 글 등록 처리 =================================================================== (파일 업로드 전)
    public void serviceBoardWrite(Board bd) {       // 리턴없음 void

        //save() : 새로운 엔티티는 저장( persist )하고 이미 있는 엔티티는 수정합니다
        //- 먼저 Entity의 @Id 프로퍼티를 찾고 해당 프로퍼티가 null이면 Transient 상태로 판단하고 null이 아니면 Detached로 판단한다.

        bdRepository.save(bd);
    }
    */



    /* 글 목록 처리 ===================================================================
    * JPA Repository
      - findBy(컬럼 이름) : 컬럼에서 키워드를 넣어서 찾겠다
      - findBy(컬럼 이름)Containing : 컬럼에서 키워드가 포함된 것을 찾겠다
    */
    public Page<Board> serviceBoardList(Pageable pageable) {         // [리턴타입] List<Board> : Entity Board 정보의 리스트를 반환함

        /*
        findAll() : 모든 엔티티를 조회합니다.
        - repository명.findAll(Sort.by(Sort.Direction.DESC, "기준컬럼명"));
        */

        //return bdRepository.findAll();
        return bdRepository.findAll(pageable);
    }

    /* 글 목록 처리 ===================================================================  페이징처리 전 (1)
    public List<Board> serviceBoardList() {         // [리턴타입] List<Board> : Entity Board 정보의 리스트를 반환함

        /*
        * JPA Repository
          - findAll() : 모든 엔티티를 조회합니다.
          - repository명.findAll(Sort.by(Sort.Direction.DESC, "기준컬럼명"));
        *

        //return bdRepository.findAll();
        return bdRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }*/


    /* 글 검색 처리 =================================================================== */
    public Page<Board> serviceBoardSearchList(String searchKeyword, Pageable pageable){
        return bdRepository.findByTitleContaining(searchKeyword, pageable);
    }


    /* 글 상세 처리 =================================================================== */
    public Board serviceBoardView(Integer id) {     // [리턴타입] Board : Entity Board
        return bdRepository.findById(id).get();
    }


    /* 글 삭제 처리 =================================================================== */
    public void serviceBoardDelete(Integer id) {    // 리턴없음 void
        bdRepository.deleteById(id);
    }

}
