package com.study.board.service;

import com.study.board.entity.Member;
import com.study.board.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private LoginRepository lgRepository;

    /* 로그인 확인 =================================================================== */
    public Member serviceLoginUserid(String userid) {
        return lgRepository.findByUserid(userid);
    }
}
