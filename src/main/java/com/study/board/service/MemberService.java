package com.study.board.service;

import com.study.board.entity.Member;
import com.study.board.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service
public class MemberService {

    @Autowired
    private MemberRepository mbRepository;

    /* 글 목록 처리 =================================================================== */
    public Page<Member> serviceMemberList(Pageable pageable) {         // [리턴타입] List<Member> : Entity Member 정보의 리스트를 반환함

        Page<Member> pagingMember = mbRepository.findAll(pageable);
        return pagingMember;
    }


    /* 글 검색 처리 =================================================================== */
    public Page<Member> serviceMemberSearchList(String userid , String username, Pageable pageable){
        Page<Member> pagingMember = mbRepository.findByUseridContainingAndUsernameContaining(userid, username, pageable);
        return pagingMember;
    }


    /* 글 등록 처리 =================================================================== */
    public void serviceMemberWrite(Member mb) {       // 리턴없음 void

        // 저장
        mbRepository.save(mb);
    }

    /* 글 상세 처리 =================================================================== */
    public Member serviceMemberView(Integer id) {     // [리턴타입] Board : Entity Board
        return mbRepository.findById(id).get();
    }

    /* 글 상세 처리 =================================================================== */
    public Integer serviceMemberUseridCheck(String userid) {     // [리턴타입] Board : Entity Board
        return mbRepository.countByUserid(userid);
    }
}
